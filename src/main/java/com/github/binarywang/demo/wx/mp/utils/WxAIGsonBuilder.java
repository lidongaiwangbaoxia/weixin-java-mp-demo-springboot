package com.github.binarywang.demo.wx.mp.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WxAIGsonBuilder {
    private static final GsonBuilder INSTANCE = new GsonBuilder();

    static {
        INSTANCE.disableHtmlEscaping();
        //配置JsonSerializer
        INSTANCE.registerTypeAdapter(WxAIError.class, new WxAIErrorAdapter());
    }
    public static Gson create() {
        return INSTANCE.create();
    }

}

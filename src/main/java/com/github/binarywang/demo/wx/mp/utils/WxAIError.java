package com.github.binarywang.demo.wx.mp.utils;

import lombok.Builder;
import lombok.Data;
import me.chanjar.weixin.common.WxType;
import me.chanjar.weixin.common.error.WxCpErrorMsgEnum;
import me.chanjar.weixin.common.error.WxError;
import me.chanjar.weixin.common.error.WxMaErrorMsgEnum;
import me.chanjar.weixin.common.error.WxMpErrorMsgEnum;
import me.chanjar.weixin.common.util.json.WxGsonBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.HashMap;

@Data
@Builder
public class WxAIError implements Serializable {
    private static final long serialVersionUID = 7859786563361406291L;

    /**
     * 返回码； 0表示成功，非0表示出错
     */
    private int ret;

    /**
     * 返回信息；ret非0时表示出错时错误原因
     */
    private String msg;

    /**
     * 返回数据；ret为0时有意义
     */
    private HashMap <String, String>data;

    private String json;
    //类似构造函数
    public static WxAIError fromJson(String json) {
        return fromJson(json, null);
    }
    // json 反序列化
    //fromJson(json, WxAIError.class) json字符串 + 需要转换对象的类型
    public static WxAIError fromJson(String json, WxType type) {
        final WxAIError wxError = WxAIGsonBuilder.create().fromJson(json, WxAIError.class);



        return wxError;
    }

    @Override
    public String toString() {
        if (this.json != null) {
            return this.json;
        }
        return "错误: Code=" + this.ret + ", Msg=" + this.msg;
    }

}

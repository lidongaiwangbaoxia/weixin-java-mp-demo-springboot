package com.github.binarywang.demo.wx.mp.controller;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.kefu.request.WxMpKfAccountRequest;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lidong   微信坑，测试账号不支持新客服功能，65400
 */
@Controller
@RequestMapping("/wx/kf/{appid}")
public class WxKeFuController {
    private WxMpService wxService;

    @Autowired
    public WxKeFuController(WxMpService wxService) {
        this.wxService = wxService;
    }
    //添加客服帐号
    @RequestMapping("/add/{kf_account}/{nickname}/{inviteWx}")
    public String addKf(@PathVariable String appid, @PathVariable String kf_account, @PathVariable String nickname, @PathVariable String inviteWx, ModelMap map) {
        if (!this.wxService.switchover(appid)) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appid));
        }

        try {
           // 接收绑定邀请的客服微信号
          //  String inviteWx ="li791506128";
            WxMpKfAccountRequest request = new WxMpKfAccountRequest(kf_account,nickname,inviteWx);
            Boolean  kf =this.wxService.getKefuService().kfAccountAdd(request);
            //map.put("user", user);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }

        return "kf_add";
    }
    //邀请绑定客服帐号
    @RequestMapping("/invite/{kf_account}/{nickname}/{inviteWx}")
    public String inviteKf(@PathVariable String appid, @PathVariable String kf_account, @PathVariable String nickname, @PathVariable String inviteWx, ModelMap map) {
        if (!this.wxService.switchover(appid)) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appid));
        }

        try {
            // 接收绑定邀请的客服微信号
            //String inviteWx ="li791506128";
            WxMpKfAccountRequest request = new WxMpKfAccountRequest(kf_account,nickname,inviteWx);
            Boolean  kf =this.wxService.getKefuService().kfAccountInviteWorker(request);
            //map.put("user", user);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }

        return "Kf_invite";
    }
}

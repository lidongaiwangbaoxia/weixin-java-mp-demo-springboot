package com.github.binarywang.demo.wx.mp.utils;

import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import com.google.gson.internal.LinkedTreeMap;
import me.chanjar.weixin.common.util.json.GsonHelper;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class WxAIErrorAdapter implements JsonDeserializer<WxAIError> {

    //反序列化需要实现的接口
    @Override
    public WxAIError deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        WxAIError.WxAIErrorBuilder errorBuilder = WxAIError.builder();
        //1.创建的是一个JsonElement对象,应Book是一个对象，所以创建一个JsonObject类型。
        JsonObject wxErrorJsonObject = json.getAsJsonObject();
        //2.将相应字段里面的数据填充到jsonObject里面
        if (wxErrorJsonObject.get("ret") != null && !wxErrorJsonObject.get("ret").isJsonNull()) {
            errorBuilder.ret(GsonHelper.getAsPrimitiveInt(wxErrorJsonObject.get("ret")));
        }
        if (wxErrorJsonObject.get("msg") != null && !wxErrorJsonObject.get("msg").isJsonNull()) {
            errorBuilder.msg(GsonHelper.getAsString(wxErrorJsonObject.get("msg")));
        }
        if (wxErrorJsonObject.get("data") != null && !wxErrorJsonObject.get("data").isJsonNull()) {

            JsonObject data= wxErrorJsonObject.get("data").getAsJsonObject();
            HashMap <String, String>data1 = new HashMap<String, String>();
            for (Map.Entry entry :data.entrySet()){
                data1.put(entry.getKey().toString(),entry.getValue().toString());
            } ;
            errorBuilder.data(data1);
        }
        errorBuilder.json(json.toString());

        return errorBuilder.build();
    }

}

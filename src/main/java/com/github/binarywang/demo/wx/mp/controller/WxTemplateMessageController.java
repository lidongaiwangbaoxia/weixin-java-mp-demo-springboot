package com.github.binarywang.demo.wx.mp.controller;


import com.github.binarywang.demo.wx.mp.utils.JsonUtils;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.kefu.request.WxMpKfAccountRequest;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateIndustry;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lidong
 * 模板消息仅用于公众号向用户发送重要的服务通知，只能用于符合其要求的服务场景中，如信用卡刷卡通知，商品购买成功通知等。
 * 不支持广告等营销类消息以及其它所有可能对用户造成骚扰的消息。
 * -----------------------------------------------------------
 * 1、模板消息调用时主要需要模板ID和模板中各参数的赋值内容；
 * 2、模板中参数内容必须以".DATA"结尾，否则视为保留字；
 * 3、模板保留符号"{{ }}"。
 * -----------------------------------------------------------
 * eg:http://3cd71853.ngrok.io/wx/templateMessage/wx17f32766bfc0a3a7/industry/2/7
 */
@Controller
@RequestMapping("/wx/templateMessage/{appid}")
public class WxTemplateMessageController {
    private WxMpService wxService;
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public WxTemplateMessageController(WxMpService wxService) {
        this.wxService = wxService;
    }

    @RequestMapping("/industry/{industry_id1}/{industry_id2}")
    public String setIndustry(@PathVariable String appid, @PathVariable String industry_id1, @PathVariable String industry_id2,  ModelMap map) {
        if (!this.wxService.switchover(appid)) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appid));
        }

        try {
            WxMpTemplateIndustry.Industry industry_1 = new WxMpTemplateIndustry.Industry(industry_id1);
            WxMpTemplateIndustry.Industry industry_2 = new WxMpTemplateIndustry.Industry(industry_id2);
            WxMpTemplateIndustry wxMpIndustry = new WxMpTemplateIndustry(industry_1,industry_2);
            Boolean  res =this.wxService.getTemplateMsgService().setIndustry(wxMpIndustry);
            //map.put("user", user);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return "industry_add";
    }

    @RequestMapping("/sendTemplateMsg/{touser}/{template_id}")
    public String sendTemplateMsg(@PathVariable String appid, @PathVariable String touser, @PathVariable String template_id,  ModelMap map) {
        if (!this.wxService.switchover(appid)) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appid));
        }

        try {
            WxMpTemplateMessage wxTemplateMessage = new WxMpTemplateMessage();
            wxTemplateMessage.setToUser(touser);
            wxTemplateMessage.setTemplateId(template_id);

            String tradeDateTime ="";
            try{
                Date date = new Date();
                tradeDateTime = new SimpleDateFormat("yyyy年MM月dd日 HH:mm").format(date);
            }catch (Exception e) {
                this. logger.error("时间转换失败！,openId:"+touser);
            }
            String type="消费";
            String number="100元";
            String outset = "尊敬的测试1，您有新的交易信息！";
            String mainBoby = "账号类型：借记卡\n" + "尾号：0001\n";
            String remark = "余额(元)：1000\n" + "备注：如有疑问，请拨打咨询热线95105678。";
            WxMpTemplateData datum1= new WxMpTemplateData("first",outset,"#173177");
            WxMpTemplateData datum2= new WxMpTemplateData("productType",mainBoby,"#173177");
            WxMpTemplateData datum3= new WxMpTemplateData("time",tradeDateTime,"#173177");
            WxMpTemplateData datum4= new WxMpTemplateData("type",type,"#173177");
            WxMpTemplateData datum5= new WxMpTemplateData("number",number,"#173177");
            WxMpTemplateData datum6= new WxMpTemplateData("remark",remark,"#173177");
            wxTemplateMessage.addData(datum1);
            wxTemplateMessage.addData(datum2);
            wxTemplateMessage.addData(datum3);
            wxTemplateMessage.addData(datum4);
            wxTemplateMessage.addData(datum5);
            wxTemplateMessage.addData(datum6);

            String msgid =this.wxService.getTemplateMsgService().sendTemplateMsg(wxTemplateMessage);
            this. logger.info("发送完成！,msgid:"+msgid);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return "industry_add";
    }
}
